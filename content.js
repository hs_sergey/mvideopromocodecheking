var ACTIVE_URL = "https://www.mvideo.ru/purchase";

var promocodesToTest = new Array();
var validPromocodes = new Array();
var expiredPromocodes = new Array();
var invalidPromocodes = new Array();
var notAllowedPromocodes = new Array();
var incorrectPromocodes = new Array();
var currentPromocodeIndex = 0;

function createPromocodeTestingButton() {
	var button = document.createElement("input");
	$(button).attr("type", "button");
	$(button).attr("id", "btnAddPromocodesToTest");
	$(button).attr("style", "margin: 20px; padding: 5px;");
	$(button).val("Добавить промокоды для тестирования...");
	$("#checkPromocodesContainer").get(0).appendChild(button);	
}

function createAddPromocodesDialog() {
	var div = document.createElement("div");
	$(div).attr("id", "addPromocodesDialog");
	$(div).attr("style", "display: none;");
	var textarea = document.createElement("textarea");
	$(textarea).attr("id", "addingPromocodes");
	$(textarea).attr("style", "width: 200px; height: 500px;");
	$(textarea).attr("placeholder", "Введите промокоды, по одному в строчке");
	div.appendChild(textarea);
	var button = document.createElement("input");
	$(button).attr("type", "button");
	$(button).attr("id", "btnStartTest");
	$(button).attr("style", "margin: 10px; padding: 5px;");
	$(button).val("Запуск тестирования");
	$(div).get(0).appendChild(button);	
	$("#checkPromocodesContainer").get(0).appendChild(div);
}

function startPromocodeTesting() {
	console.log("startPromocodeTesting");
	currentPromocodeIndex = 0;
	validPromocodes = new Array();
	localStorage.setItem("validPromocodes", implode("\n", validPromocodes));
	expiredPromocodes = new Array();
	invalidPromocodes = new Array();
	notAllowedPromocodes = new Array();
	incorrectPromocodes = new Array();
	localStorage.setItem("invalidPromocodes", implode("\n", invalidPromocodes));
	localStorage.setItem("expiredPromocodes", implode("\n", expiredPromocodes));
	localStorage.setItem("notAllowedPromocodes", implode("\n", notAllowedPromocodes));
	localStorage.setItem("state", "running");
	localStorage.setItem("currentPromocodeIndex", currentPromocodeIndex);
	localStorage.setItem("promocodesToTest", implode("\n", promocodesToTest));
	localStorage.setItem("incorrectPromocodes", implode("\n", incorrectPromocodes));
	checkPromocode1();
}

function checkPromocode1() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("checkPromocode1");
	console.log("cheking promocode " + (currentPromocodeIndex+1)  + " of " + promocodesToTest.length);
	$('input[name="promocodeOrCardNumber"]').val(promocodesToTest[currentPromocodeIndex]);
	setTimeout(checkPromocode2, 1000);
}

//function checkPromocode1Result() {
//	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
//		console.log("exiting case url changed");
//		return;
//	}
//	if($(".o-promo-code__btn").length == 0) {
//		console.log("checkPromocode1 failed");
//		//отменяем выбор
//		$("label.c-checkbox__label").get(0).click();
////		$('a[data-js="edit_promocode"]')[0].click();
//		setTimeout(checkPromocode1Result2, 3000);
//	} else {
//		checkPromocode2();
//	}
//}
//
//function checkPromocode1Result2() {
//	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
//		console.log("exiting case url changed");
//		return;
//	}
//	$("label.c-checkbox__label").get(0).click();
//	setTimeout(checkPromocode4Result, 1000);	
//}

function checkPromocode2() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("checkPromocode2");
	if($("#recaptcha-promocode").length) {
		console.log("captcha detected!");
		var button = document.createElement("input");
		$(button).attr("type", "button");
		$(button).attr("id", "btnContinueAfterCaptha");
		$(button).attr("style", "margin: 20px; padding: 5px;");
		$(button).val("Продолжить");
		insertAfter(button, $("#recaptcha-promocode").get(0));
		
		$("#btnContinueAfterCaptha").click(function() {
			$("#btnContinueAfterCaptha").remove();
			$('button[data-url="applyPromocodeOrVipCard"]')[0].click();
			setTimeout(checkPromocode2Result, 1000);
		});
		
		return;
	}
	
	
	// $('button[data-url="applyPromocodeOrVipCard"]').prop("disabled", false);
	$('button[data-url="applyPromocodeOrVipCard"]')[0].click();
// 	$("#promoConfigPromotionCode").submit();
	setTimeout(checkPromocode2Result, 1000);
}


function insertAfter(elem, refElem) {
	return refElem.parentNode.insertBefore(elem, refElem.nextSibling);
}

function checkPromocode2Result() {
	//необходимо понять, была ли уже закончена проверка. 
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	if($('span[data-sel="new_checkout-span-promocode_result"]').is(':visible') || $('label[data-sel="new_checkout-label-promocode_error"]').is(':visible')) {
		setTimeout(checkPromocode3, 500);
	} else {
		console.log("checkPromocode2Result: checking is still processing");
		setTimeout(checkPromocode2Result, 1000);
	}


	// //при вводе промокода o-promo-code clearfix o-available-stock__item
	// //для успешного промокода o-promo-code clearfix o-available-stock__item o-promo-code_success-code-state
	// //для инвалидных o-promo-code clearfix o-available-stock__item hidden
	// var classes = $(".o-promo-code").attr("class").split(' ');
	// console.log(classes);
	// if(classes.length >= 3) {
	// 	//cheking expired promocode
	// 	var foundInvalid = false;
	// 	$('label[data-sel="new_checkout-label-promocode_error"]').each(function() {
	// 		var value = $(this).html();
	// 		if(value.indexOf("Промокод уже был использован") !== -1) {
	// 			foundInvalid = true;
	// 		}
	// 	});
	// 	var foundExpired = false;
	// 	$('label[data-sel="new_checkout-label-promocode_error"]').each(function() {
	// 		var value = $(this).html();
	// 		if(value.indexOf("Срок действия промокода истёк") !== -1) {
	// 			foundExpired = true;
	// 		}
	// 	});
	// 	var foundNotAllowed = false;
	// 	$('label[data-sel="new_checkout-label-promocode_error"]').each(function() {
	// 		var value = $(this).html();
	// 		if(value.indexOf("Промокод невозможно применить ни к одному товару в заказе") !== -1) {
	// 			foundNotAllowed = true;
	// 		}
	// 	});
	// 	var foundIncorrect = false;
	// 	$('label[data-sel="new_checkout-label-promocode_error"]').each(function() {
	// 		var value = $(this).html();
	// 		if(value.indexOf("Промокод некорректен") !== -1) {
	// 			foundIncorrect = true;
	// 		}
	// 	});
	// 	var foundDiscount = false;
	// 	$(".o-promo-code__result").each(function() {
	// 		var value = $(this).html();
	// 		if(value.indexOf("Скидка") !== -1) {
	// 			foundDiscount = true;
	// 		}
	// 	});
	// 	if(foundDiscount|| foundInvalid || foundExpired || foundNotAllowed || foundIncorrect) {
	// 		setTimeout(checkPromocode3, 500);
	// 	} else {
	// 		console.log("checkPromocode2Result: checking is still processing");
	// 		setTimeout(checkPromocode2Result, 1000);
	// 	}
	// } else {
	// 	setTimeout(checkPromocode3, 500);
	// }
}


function checkPromocode3() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("checkPromocode3");
	var foundDiscount = false;
	$(".o-promo-code__result").each(function() {
		var value = $(this).html();
		if(value.indexOf("Скидка") !== -1) {
			foundDiscount = true;
		}
	});
	if(foundDiscount) {
		console.log("promocode " + promocodesToTest[currentPromocodeIndex] + " is VALID");
		validPromocodes.push(promocodesToTest[currentPromocodeIndex]);
		localStorage.setItem("validPromocodes", implode("\n", validPromocodes));
		//отменяем выбор
		$("label.c-checkbox__label").get(1).click();
		// $('a[data-js="edit_promocode"]')[0].click();
		setTimeout(checkPromocode3Result1, 3000);
	} else {
		//cheking expired promocode
		var foundExpired = false;
		$('label[data-sel="new_checkout-label-promocode_error"]').each(function() {
			var value = $(this).html();
			if(value.indexOf("Срок действия промокода истёк") !== -1) {
				foundExpired = true;
			}
		});
		if(foundExpired) {
			console.log("promocode " + promocodesToTest[currentPromocodeIndex] + " is EXPIRED");
			expiredPromocodes.push(promocodesToTest[currentPromocodeIndex]);
			localStorage.setItem("expiredPromocodes", implode("\n", expiredPromocodes));
			currentPromocodeIndex++;
			localStorage.setItem("currentPromocodeIndex", currentPromocodeIndex);
			if(currentPromocodeIndex >= promocodesToTest.length) {
				localStorage.setItem("state", "finished");
				handleAllPromocodesChecked();
			} else {
				localStorage.setItem("state", "checkPromocode3Result2");
				// location.reload(true);
//				//отменяем выбор
				$("label.c-checkbox__label").get(1).click();
				// $('a[data-js="edit_promocode"]')[0].click();
				setTimeout(checkPromocode3Result2, 1000);
			}			
		} else {
			//checking not allowed promocodes
			var foundNotAllowed = false;
			$('label[data-sel="new_checkout-label-promocode_error"]').each(function() {
				var value = $(this).html();
				if(value.indexOf("Промокод невозможно применить ни к одному товару в заказе") !== -1) {
					foundNotAllowed = true;
				}
			});
			if(foundNotAllowed) {
				console.log("promocode " + promocodesToTest[currentPromocodeIndex] + " is NOT ALLOWED");
				notAllowedPromocodes.push(promocodesToTest[currentPromocodeIndex]);
				localStorage.setItem("notAllowedPromocodes", implode("\n", notAllowedPromocodes));
				currentPromocodeIndex++;
				localStorage.setItem("currentPromocodeIndex", currentPromocodeIndex);
				if(currentPromocodeIndex >= promocodesToTest.length) {
					localStorage.setItem("state", "finished");
					handleAllPromocodesChecked();
				} else {
					localStorage.setItem("state", "checkPromocode3Result2");
					// location.reload(true);
//
					//отменяем выбор
					$("label.c-checkbox__label").get(1).click();
					// $('a[data-js="edit_promocode"]')[0].click();
					setTimeout(checkPromocode3Result2, 1000);
				}			
			} else {
				//checking incorrect
				var foundIncorrect = false;
				$('label[data-sel="new_checkout-label-promocode_error"]').each(function() {
					var value = $(this).html();
					if(value.indexOf("Промокод некорректен") !== -1) {
						foundIncorrect = true;
					}
				});
				if(foundIncorrect) {
					console.log("promocode " + promocodesToTest[currentPromocodeIndex] + " is INCORRECT");
					incorrectPromocodes.push(promocodesToTest[currentPromocodeIndex]);
					localStorage.setItem("incorrectPromocodes", implode("\n", incorrectPromocodes));
					currentPromocodeIndex++;
					localStorage.setItem("currentPromocodeIndex", currentPromocodeIndex);
					if(currentPromocodeIndex >= promocodesToTest.length) {
						localStorage.setItem("state", "finished");
						handleAllPromocodesChecked();
					} else {
						localStorage.setItem("state", "checkPromocode3Result2");
						// location.reload(true);
						//отменяем выбор
						$("label.c-checkbox__label").get(1).click();
						// $('a[data-js="edit_promocode"]')[0].click();
						setTimeout(checkPromocode3Result2, 1000);
					}			
				} else {				
					console.log("promocode " + promocodesToTest[currentPromocodeIndex] + " is INVALID");
					invalidPromocodes.push(promocodesToTest[currentPromocodeIndex]);
					localStorage.setItem("invalidPromocodes", implode("\n", invalidPromocodes));
					currentPromocodeIndex++;
					localStorage.setItem("currentPromocodeIndex", currentPromocodeIndex);
					if(currentPromocodeIndex >= promocodesToTest.length) {
						localStorage.setItem("state", "finished");
						handleAllPromocodesChecked();
					} else {
						localStorage.setItem("state", "checkPromocode3Result2");
						// location.reload(true);
						//отменяем выбор
						$("label.c-checkbox__label").get(1).click();
						// $('a[data-js="edit_promocode"]')[0].click();
						setTimeout(checkPromocode3Result2, 1000);
					}
				}
			}
		}
	}
}

function checkPromocode3Result1() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	if($('a[data-js="edit_promocode"]').length > 0) {
		console.log("checkPromocode3Result1 failed - still processing");
//		$('a[data-js="edit_promocode"]')[0].click();
		setTimeout(checkPromocode3Result1, 3000);
	} else {
		checkPromocode4();
	}
}

function checkPromocode3Result2() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	localStorage.setItem("state", "running");
	if($('input[name="promocodeOrCardNumber"]').length == 0) {
		console.log("checkPromocode3Result2 failed - still processing");
		setTimeout(checkPromocode3Result2, 3000);
	} else {
		$("label.c-checkbox__label").get(1).click();
		setTimeout(checkPromocode4Result, 1000);
//		checkPromocode1();
	}
}

function checkPromocode4() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("checkPromocode4");
	currentPromocodeIndex++;
	localStorage.setItem("currentPromocodeIndex", currentPromocodeIndex);
	if(currentPromocodeIndex >= promocodesToTest.length) {
		localStorage.setItem("state", "finished");
		handleAllPromocodesChecked();
	} else {
		$("label.c-checkbox__label").get(1).click();
		setTimeout(checkPromocode4Result, 1000);
	}
}

function checkPromocode4Result() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	if($('input[name="promocodeOrCardNumber"]').length == 0) {
		console.log("checkPromocode4 failed");
		setTimeout(checkPromocode4Result, 1000);
	} else {
		checkPromocode1();
	}
}



function handleAllPromocodesChecked() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("all promocodes are checked");
	if($("#checkPromocodesContainer").length) {
		div = $("#checkPromocodesContainer").get(0);
		$(div).html("");
	} else {
		div = document.createElement("div");
		$(div).attr("id", "checkPromocodesContainer");
		$(div).attr("style", "margin: 20px; padding: 10px; border: solid 1px blue;");
	}
	$(div).attr("id", "checkPromocodesContainer");
	$(div).attr("style", "margin: 20px; padding: 10px; border: solid 1px blue;");
	var label = document.createElement("label");
	$(label).attr("style", "margin-top: 20px");
	$(label).html("Валидные промокоды:");
	div.appendChild(label);
	var textarea = document.createElement("textarea");
	$(textarea).attr("style", "width: 200px; height: 500px;");
	$(textarea).html(implode("\n", validPromocodes));
	div.appendChild(textarea);
	label = document.createElement("label");
	$(label).attr("style", "margin-top: 20px");
	$(label).html("Невалидные промокоды:");
	div.appendChild(label);
	textarea = document.createElement("textarea");
	$(textarea).attr("style", "width: 200px; height: 500px;");
	$(textarea).html(implode("\n", invalidPromocodes));
	div.appendChild(textarea);
	label = document.createElement("label");
	$(label).attr("style", "margin-top: 20px");
	$(label).html("Истекшие промокоды:");
	div.appendChild(label);
	textarea = document.createElement("textarea");
	$(textarea).attr("style", "width: 200px; height: 200px;");
	$(textarea).html(implode("\n", expiredPromocodes));
	div.appendChild(textarea);
	
	label = document.createElement("label");
	$(label).attr("style", "margin-top: 20px");
	$(label).html("Промокоды, которые невозможно применить ни к одному товару:");
	div.appendChild(label);
	textarea = document.createElement("textarea");
	$(textarea).attr("style", "width: 200px; height: 200px;");
	$(textarea).html(implode("\n", notAllowedPromocodes));
	div.appendChild(textarea);

	label = document.createElement("label");
	$(label).attr("style", "margin-top: 20px");
	$(label).html("Некорректные промокоды:");
	div.appendChild(label);
	textarea = document.createElement("textarea");
	$(textarea).attr("style", "width: 200px; height: 200px;");
	$(textarea).html(implode("\n", incorrectPromocodes));
	div.appendChild(textarea);
	
	var button = document.createElement("input");
	$(button).attr("type", "button");
	$(button).attr("id", "btnNewTest");
	$(button).attr("style", "margin: 10px; padding: 5px;");
	$(button).val("Начать заново");
	div.appendChild(button);
	$(".o-checkout__step-notification").get(0).appendChild(div);
	
	$("#btnNewTest").click(function() {
		startNewTest();
	});

}

function implode( glue, pieces ) {	// Join array elements with a string
	// 
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: _argos
	return ( ( pieces instanceof Array ) ? pieces.join ( glue ) : pieces );
}

function startNewTest() {
	$("#checkPromocodesContainer").html("");
	createAddPromocodesDialog();
//	createPromocodeTestingButton();
	$("#addPromocodesDialog").show();
//	$("#btnAddPromocodesToTest").hide();
	
//	$("#btnAddPromocodesToTest").click(function() {
//		console.log("add promocodes to test click");
//		$("#addPromocodesDialog").show();
//		$("#btnAddPromocodesToTest").hide();
//	});
	
	$("#btnStartTest").click(function() {
		var promocodes = $("#addingPromocodes").val();
		if(promocodes) {
			promocodesToTest = promocodes.split("\n");
			console.log("found " + promocodesToTest.length + " promocodes to test");
			$("label.c-checkbox__label").get(1).click();
			setTimeout(startPromocodeTesting, 3000);
		}
	});
	
}

$(document).ready(function() {
	console.log("content js document ready");
	var savedState = localStorage.getItem("state");
	currentPromocodeIndex = parseInt(localStorage.getItem("currentPromocodeIndex"));
	var savedPromocodesToTest = localStorage.getItem("promocodesToTest");
	if(savedPromocodesToTest) {
		promocodesToTest = savedPromocodesToTest.split("\n");
	}
	var savedValidPromocodes = localStorage.getItem("validPromocodes");
	if(savedValidPromocodes) {
		validPromocodes = savedValidPromocodes.split("\n");
	}
	var savedInvalidPromocodes = localStorage.getItem("invalidPromocodes");
	if(savedInvalidPromocodes) {
		invalidPromocodes = savedInvalidPromocodes.split("\n");
	}
	var savedExpiredPromocodes = localStorage.getItem("expiredPromocodes");
	if(savedExpiredPromocodes) {
		expiredPromocodes = savedExpiredPromocodes.split("\n");
	}
	var savedNotAllowedPromocodes = localStorage.getItem("notAllowedPromocodes");
	if(savedNotAllowedPromocodes) {
		notAllowedPromocodes = savedNotAllowedPromocodes.split("\n");
	}
	var savedIncorrectPromocodes = localStorage.getItem("incorrectPromocodes");
	if(savedIncorrectPromocodes) {
		incorrectPromocodes = savedIncorrectPromocodes.split("\n");
	}
	if(savedState == "finished") {
		handleAllPromocodesChecked();
	} else if(savedState == "checkPromocode3Result2") {
		checkPromocode3Result2();
	} else if(savedState == "running") {
		var div = document.createElement("div");
		$(div).attr("id", "checkPromocodesContainer");
		$(div).attr("style", "margin: 20px; padding: 10px; border: solid 1px blue;");
		$(".o-checkout__step-notification").get(0).appendChild(div);
		
		var label = document.createElement("label");
		$(label).html("Предыдущая проверка была прервана! Проверено " + currentPromocodeIndex + " промокодов из " + promocodesToTest.length);
		$("#checkPromocodesContainer").get(0).appendChild(label);
		
		var button = document.createElement("input");
		$(button).attr("type", "button");
		$(button).attr("id", "btnContinueTest");
		$(button).attr("style", "margin: 10px; padding: 5px;");
		$(button).val("Продолжить");
		$("#checkPromocodesContainer").get(0).appendChild(button);
		
		button = document.createElement("input");
		$(button).attr("type", "button");
		$(button).attr("id", "btnNewTest");
		$(button).attr("style", "margin: 10px; padding: 5px;");
		$(button).val("Начать заново");
		$("#checkPromocodesContainer").get(0).appendChild(button);
		
		$("#btnContinueTest").click(function() {
			$("#checkPromocodesContainer").html("");
			$("label[for='promocode']").click();
			setTimeout(checkPromocode1, 3000);
		});

		$("#btnNewTest").click(function() {
			startNewTest();
		});
	} else {
		var div = document.createElement("div");
		$(div).attr("id", "checkPromocodesContainer");
		$(div).attr("style", "margin: 20px; padding: 10px; border: solid 1px blue;");
		$(".o-checkout__step-notification").get(0).appendChild(div);
		startNewTest();
	}
	
});
